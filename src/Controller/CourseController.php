<?php

namespace App\Controller;

use App\Entity\Course;
use App\Form\CourseType;
use App\Repository\CourseRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CourseController extends AbstractController
{
    #[Route('/course', name: 'app_course')]
    public function index(): Response
    {
        return $this->render('course/index.html.twig', [
            'controller_name' => 'CourseController',
        ]);
    }
    #[Route('/demo', name: 'app_course_demo',methods: ['GET'])]
    public function demo(EntityManagerInterface $em):Response{
        //Créer une instance de l'entité
        $course = new Course();
        //hydrater toutes les propriétés
        $course->setName('Symfony');
        $course->setContent('Le développement Web côté Serveur (avec Symfony)');
        $course->setDuration(10);
        //Récupération de la date et l'heure du  jour
        $course->setDateCreated(new \DateTimeImmutable());
        $em->persist($course);

        dump($course);
        //Penser au flush afin d'enregistrer l'objet en base de données.
        $em->flush();
        dump($course);

        //On modifie l'objet
        $course->setName('PHP');

        //On sauvegarde l'objet
        $em->flush($course); //Apres chaque flush une nouvelle transaction est démarrée.
        dump($course);

        //On supprime l'objet
        $em->remove($course);
        $em->flush();

        return $this->render('course/create.html.twig');
    }
    #[Route('/list', name: 'app_course_list',methods: ['GET'])]
    public function list(CourseRepository $courseRepository):Response{
        //Appel du repository équivalent à la DAL de Cours.
        //Requête pour récupérer l'ensemble des cours.
        //Mise à jour de la variable $courses qui contiendra l'ensemble des cours de la BD.
       // $courses =$courseRepository->findAll();
        //$courses =$courseRepository->findBy([],['name'=>'DESC'],5);

        $courses = $courseRepository->findLastCourses();
        //Appel à la vue en passant le parametre courses qui permettra d'afficher la liste des cours dans la vue.
        return $this->render('course/list.html.twig',['courses'=>$courses]);
    }

    #[Route('/{id}', name: 'app_course_show',requirements:['id'=>'\d+'], methods: ['GET'])]
    public function show(int $id,CourseRepository $courseRepository):Response{
        //Récupération du cours grace à son identifiant. On va chercher dans la BD par rapport à son id.
        $course=$courseRepository->find($id);
        //Si on ne trouve pas le cours message d'erreur
        if(!$course){
            throw $this->createNotFoundException('Cours inconnu');
        }
        //Sinon on affiche le cours
        return $this->render('course/show.html.twig',['course'=>$course]);
    }

    #[Route('/ajouter', name: 'app_course_create', methods: ['GET','POST'])]
    public function create(Request $request,EntityManagerInterface $em): Response{
        //Creation de l'objet
        $course = new Course();
        //Création du formulaire et association de l'objet au formulaire
        $courseform = $this->createForm(CourseType::class,$course);
        //TODO : Traiter le formulaire
        //1er etape pour le traitement du formulaire
        $courseform->handleRequest($request);
        //Tester si le formulaire a été soumis. J'ai bien cliqué sur le bouton de soumission du formulaire.
        //Je vérifie que le formulaire respecte les regles de gestion.
        if($courseform->isSubmitted() && $courseform->isValid()){
            //Traitement des données
            $em->persist($course);
            $em->flush();

            //Message flash
            $this->addFlash('success','Cours créé !');
            //Redirection vers une autre page
            //return $this->redirectToRoute('app_course');
            return $this->redirectToRoute('app_course_show',['id'=>$course->getId()]);
        }

        return $this->renderForm('course/create.html.twig',['courseForm'=>$courseform]);
    }


    #[Route('/{id}/modifier', name: 'app_course_edit', requirements:['id'=>'\d+'], methods: ['GET','POST'])]
    public function edit(int $id,Request $request,EntityManagerInterface $em): Response
    {
        $course = $em->getRepository(Course::class)->find($id);
        //Création du formulaire et association de l'objet au formulaire
        $courseform = $this->createForm(CourseType::class,$course);

        //1er etape pour le traitement du formulaire
        $courseform->handleRequest($request);

        //Tester si le formulaire a été soumis. J'ai bien cliqué sur le bouton de soumission du formulaire.
        //Je vérifie que le formulaire respecte les regles de gestion.
        if($courseform->isSubmitted() && $courseform->isValid()){
            //Traitement des données
            //le persist() n'est pas nécessaire car Doctrine connait deja l'objet.
            $em->flush();

            //Message flash
            $this->addFlash('success','Cours modifié !');
            //Redirection vers une autre page
            return $this->redirectToRoute('app_course_show',['id'=>$course->getId()]);
        }
        return $this->renderForm('course/edit.html.twig',['courseForm'=>$courseform]);
    }
}









