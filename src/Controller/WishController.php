<?php

namespace App\Controller;

use App\Entity\Wish;
use App\Form\WishType;
use App\Repository\WishRepository;
use App\Service\FileUploader;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class WishController extends AbstractController
{
    #[Route('/wish', name: 'app_wish')]
    public function index(): Response
    {
        return $this->render('wish/index.html.twig', [
            'controller_name' => 'WishController',
        ]);
    }

    #[Route('/wishes/create', name: 'app_wish_create',methods: ['GET','POST'])]
    public function create(Request $request,EntityManagerInterface $em,FileUploader $fileUploader): Response{
        $wish=new wish();
        //Associer le formulaire à l'entité.
        $wishForm = $this->createForm(WishType::class,$wish);
        //Récupere les données du form et les injecte dans $wish.
        $wishForm->handleRequest($request);
        //On teste si le formulaire est soumis et valide
        if($wishForm->isSubmitted() && $wishForm->isValid()){
            //Traitement de l'image
            $imageFile=$wishForm->get('image')->getData();
            if($imageFile){
                $wish->setFilename($fileUploader->upload($imageFile));
            }
            //hydrate les propriétés absentes du formulaire
            $wish->setIsPublished(true);
            //sauvegarde en bdd
            $em->persist($wish);
            $em->flush();
            //Affiche le message sur la prochaine page
            $this->addFlash('success','Idée créée avec succès !');
            //redirige vers la page de de détail fraichement créée.
            return $this->redirectToRoute('app_wish_detail',['id'=>$wish->getId()]);
        }

        //affiche le formulaire
        return $this->renderForm('wish/create.html.twig',['wishForm'=>$wishForm]);
    }

    #[Route('/wishes/{id}/update', name: 'app_wish_update', requirements:['id'=>'\d+'],
        methods: ['GET','POST'])]
    public function update(int $id, WishRepository $wishRepository, Request $request,EntityManagerInterface $em
    ,FileUploader $fileUploader): Response{
        //Récupére le whish en fonction de l'id présent dans l'URL
        $wish=$wishRepository->find($id);
        if(!$wish){
            throw $this->createNotFoundException('Ce wish n\'existe pas !');
        }
        //Associer le formulaire à l'entité.
        $wishForm = $this->createForm(WishType::class,$wish);
        //Récupere les données du form et les injecte dans $wish.
        $wishForm->handleRequest($request);
        //On teste si le formulaire est soumis et valide
        if($wishForm->isSubmitted() && $wishForm->isValid()){
            //Traitement de l'image
            $imageFile=$wishForm->get('image')->getData();
            if($imageFile || (($wishForm->has('deleteImage') && $wishForm['deleteImage']->getData()))){
                //Suppression de l'ancienne image
                //Si on a coché l'option dans le formulaire
                //Ou si on a changé d'image
                $fileUploader->delete($wish->getFilename(),$this->getParameter('targetDirectory'));
                if($imageFile){
                    //cas ou on a une nouvelle image à sauvgarder
                    $wish->setFilename($fileUploader->upload($imageFile));

                }else{
                    //Cas ou on supprime l'image sans uploader une nouvelle image
                    $wish->setFilename(null);
                }
                $wish->setFilename($fileUploader->upload($imageFile));
            }


            //hydrate les propriétés absentes du formulaire
            $wish->setIsPublished(true);
            $em->flush();
            //Affiche le message sur la prochaine page
            $this->addFlash('success','Idée mise à jour avec succès !');
            //redirige vers la page de de détail fraichement créée.
            return $this->redirectToRoute('app_wish_detail',['id'=>$wish->getId()]);
        }

        //affiche le formulaire
        return $this->renderForm('wish/create.html.twig',['wishForm'=>$wishForm]);
    }
    #[Route('/wishes/{id}/delete', name: 'app_wish_delete', requirements:['id'=>'\d+'],
        methods: ['GET'])]
    public function delete(int $id, WishRepository $wishRepository, Request $request): Response
    {
        //Récupére le whish en fonction de l'id présent dans l'URL
        $wish = $wishRepository->find($id);
        if (!$wish) {
            throw $this->createNotFoundException('Ce wish n\'existe pas !');
        }
        if($this->isCsrfTokenValid('delete'.$id,$request->query->get('token'),)){
            $wishRepository->remove($wish,true);
            $this->addFlash('success','Le wish a été supprimée');
        }else{
            $this->addFlash('danger','Impossible de supprimer');
        }
        //On retourne vers la liste
        return $this->renderForm('wish/index.html.twig');
    }


}
