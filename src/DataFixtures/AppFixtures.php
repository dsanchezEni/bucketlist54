<?php

namespace App\DataFixtures;

use App\Entity\Course;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture  implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        // $product = new Product();
        // $manager->persist($product);
       /*
        $course = new Course();
        //hydrater toutes les propriétés
        $course->setName("JAVA");
        $course->setContent('Le développement Web côté Serveur (avec JAVA)');
        $course->setDuration(10);
        //Récupération de la date et l'heure du  jour
        $course->setDateCreated(new \DateTimeImmutable());
        $course->setCategory($this->getReference('category1'));
        $manager->persist($course);

        $coursePOO = new Course();
        //hydrater toutes les propriétés
        $coursePOO->setName("POO");
        $coursePOO->setContent('Programmation Orienté Objet');
        $coursePOO->setDuration(10);
        //Récupération de la date et l'heure du  jour
        $coursePOO->setDateCreated(new \DateTimeImmutable());
        $coursePOO->setCategory($this->getReference('category1'));
        $manager->persist($coursePOO);

        $courseHTML = new Course();
        //hydrater toutes les propriétés
        $courseHTML->setName("HTML");
        $courseHTML->setContent('HTML et CSS');
        $courseHTML->setDuration(5);
        //Récupération de la date et l'heure du  jour
        $courseHTML->setDateCreated(new \DateTimeImmutable());
        $courseHTML->setCategory($this->getReference('category1'));
        $manager->persist($courseHTML);

        //Utilisation de 30 cours supplémentaire
        //Sans l'utilisation de Faker
        for ($i=1;$i<=30;$i++){
            $course = new Course();
            $course->setName("Cours $i");
            $course->setContent("Description du cours $i");
            //Random de 1 à 10
            $course->setDuration(mt_rand(1,10));
            //Récupération de la date et l'heure du  jour
            $course->setDateCreated(new \DateTimeImmutable());
            if($i%2 == 0) {
                $course->setCategory($this->getReference('category1'));
            }
            else{
                $course->setCategory($this->getReference('category2'));
            }
            $manager->persist($course);
        }

        //Utilisation de Faker
        $faker = \Faker\Factory::create('fr_FR');
        for ($i=1;$i<=30;$i++){
            $course = new Course();
            $course->setName($faker->word());
            $course->setContent($faker->realText());
            //Random de 1 à 10
            $course->setDuration(mt_rand(1,10));
            $course->setPublished($faker->boolean(50));
            //Récupération de la date et l'heure du  jour
            $dateCreated = $faker->dateTimeBetween('-2 months','now');
            //nb: dateTimeBetween() retourne un DateTime, il faut donc le convertir en DateTimeImmutable
            $course->setDateCreated(\DateTimeImmutable::createFromMutable($dateCreated));

            $dateModified = $faker->dateTimeBetween($course->getDateCreated()->format('Y-m-d'),'now');
            $course->setDateModified(\DateTimeImmutable::createFromMutable($dateModified));
            $course->setCategory($this->getReference('category'.mt_rand(1,2)));
            $manager->persist($course);
        }*/

        $manager->flush();
    }

    public function getDependencies()
    {
        return [CourseFixtures::class];
    }
}
