<?php

namespace App\Repository;

use App\Entity\Course;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Course>
 *
 * @method Course|null find($id, $lockMode = null, $lockVersion = null)
 * @method Course|null findOneBy(array $criteria, array $orderBy = null)
 * @method Course[]    findAll()
 * @method Course[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CourseRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Course::class);
    }

    public function save(Course $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Course $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @return Course[]
     *
     */
    public function findLastCourses(int $duration=2) :array
    {
//        //En DQL
//        $entityManager = $this->getEntityManager();
//        //Ecriture de la requete en DQL donc on pense OBJET !!!!
//        $dql = "SELECT c
//                FROM App\Entity\Course c
//                WHERE c.duration > :duration
//                ORDER BY  c.dateCreated DESC";
//        //A partir de l'entity Manager on crée la requete en faisant appel au DQL
//        $query = $entityManager->createQuery($dql);
//        //Combien de résultat on souhaite.
//        $query->setParameter('duration',$duration)
//              ->setMaxResults(5);
//
//        return $query->getResult();

        //version QueryBuilder
        $queryBuilder = $this->createQueryBuilder('c')
            //On pense Objet ! Pas d'importance sur l'ordre dans le queryBuilder
            ->andWhere('c.duration > :duration')
            ->addOrderBy('c.dateCreated', 'DESC')
            ->setParameter('duration', $duration)
            ->setMaxResults(5);
        $query = $queryBuilder->getQuery();
        return $query->getResult();
    }

//    /**
//     * @return Course[] Returns an array of Course objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('c.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Course
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
