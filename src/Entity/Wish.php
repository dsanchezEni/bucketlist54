<?php

namespace App\Entity;

use App\Repository\WishRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: WishRepository::class)]
class Wish
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Assert\NotBlank(message:'Veuillez renseigner une idée !')]
    #[Assert\Length(
        min:5,
        minMessage:'Minimum {{limit}} caractères s\'il vous plait!',
        max:255,
        maxMessage:'Maximum {{limit}} caractères s\'il vous plait!',
    )]
    private ?string $titre = null;

    #[ORM\Column(length: 50)]
    #[Assert\NotBlank(message:'Veuillez renseigner un auteur !')]
    #[Assert\Length(
        min:3,
        minMessage:'Minimum {{limit}} caractères s\'il vous plait!',
        max:50,
        maxMessage:'Maximum {{limit}} caractères s\'il vous plait!',
    )]
    #[Assert\Regex(
        pattern:'/^[a-z0-9-_-]+$/i',
        message: 'Veuillez utiliser seulement des lettres des nombres et des underscores !'
    )]
    private ?string $author = null;

    private ?bool $published = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $description = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $filename = null;


    public function __construct()
    {
        $this->published = false;
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): static
    {
        $this->titre = $titre;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): static
    {
        $this->description = $description;

        return $this;
    }

    public function getFilename(): ?string
    {
        return $this->filename;
    }

    public function setFilename(?string $filename): static
    {
        $this->filename = $filename;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getAuthor(): ?string
    {
        return $this->author;
    }

    /**
     * @param string|null $author
     * @return Wish
     */
    public function setAuthor(?string $author): Wish
    {
        $this->author = $author;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getPublished(): ?bool
    {
        return $this->published;
    }

    /**
     * @param bool|null $published
     * @return Wish
     */
    public function setPublished(?bool $published): Wish
    {
        $this->published = $published;
        return $this;
    }

}
