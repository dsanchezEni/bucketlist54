<?php

namespace App\Form;

use App\Entity\Course;

use phpDocumentor\Reflection\Types\Integer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CourseType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
       /* $builder
            ->add('name')
            ->add('content')
            ->add('duration')
            ->add('published')
            ->add('dateCreated')
            ->add('dateModified')
        ;*/
        $builder
            ->add('name',TextType::class,['label'=>'Titre'])
            //required à false car on a autorisé des valeurs nulles sur content.
            ->add('content',TextareaType::class,['label'=>'Description','required'=>false])
            ->add('duration',IntegerType::class,['label'=>'Durée (jours)'])
            //1er solution pour ajouter le bouton.
            //->add('btnCreate',SubmitType::class,['label'=>'Ajouter'])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            //Association du formulaire à l'entité Course
            'data_class' => Course::class,
        ]);
    }
}
